import React, { Component } from "react";
import openSocket from "socket.io-client";

let socket = null;
let mediaStream = null;
let audio_context = null;
let isRecordingClick = null;
var osType = "unknown OS";

if (navigator.appVersion.indexOf("Win") !== -1) osType = "Windows";
if (navigator.appVersion.indexOf("Mac") !== -1) osType = "MacOS";
if (navigator.appVersion.indexOf("X11") !== -1) osType = "UNIX";
if (navigator.appVersion.indexOf("Linux") !== -1) osType = "Linux";

class GnaniEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editorState: null,
      currentValue: 0,
      isToggled: false,
      input: "",
      isKeyboardOn: true,
      isVoiceOn: true,
      isMicOn: false,
      keyboardLayoutLanguage: "hindi",
      keyboardLanguage: "Hindi",
      layoutName: "default",
      keyBindingFn: this.hindiKeyBinding,
      namespace: "/test",
      selectedVoiceLanguage: "Hindi",
      micImage: null,
      saveStatus: "",
      transcript: this.props.transcript,
      editorContentHtml: "",
      resultText: "",
      text: "",
      voiceArray: [
        "Hindi",
        "English",
        "Kannada",
        "Telugu",
        "Tamil",
        "Gujrathi"
      ],
      keyBoardArray: ["Hindi", "English", "Kannada", "Tamil", "Nudi"],
      transliterationArray: ["Hindi", "English"]
    };

    this.onEditorStateChange = editorState => {
      this.setState({ editorState });
    };
  }
  //Recorder functions
  handleToggle = () => {
    isRecordingClick = !isRecordingClick;
    if (isRecordingClick) {
      this.setState({
        isMicOn: true
      });
      //console.log('Start record');

      this.handleStartRecording();
    } else {
      this.setState({
        isMicOn: false
      });
      ////console.log('Stop record');
      this.handleStopRecording();
    }
  };
  componentDidMount() {
    isRecordingClick = false;
    window.scrollTo(0, 0);
    socket = openSocket("https://editor.gnani.ai");

    console.log("OS: ", osType);
  }

  componentWillUnmount() {
    socket.emit("disconnect");
    socket = null;
  }

  onChange = editorState => {
    this.setState({
      editorState,
      saveStatus: "All changes saved"
    });
  };

  blobToFile = (blob, fileName) => {
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    blob.lastModifiedDate = new Date();
    blob.name = fileName;
    return blob;
  };

  // // audio functions
  initializeRecorder = stream => {
    // https://stackoverflow.com/a/42360902/4666b93
    mediaStream = stream;

    // get sample rate
    audio_context = new AudioContext();

    var audioInput = audio_context.createMediaStreamSource(stream);

    // ////console.log("Created media stream.");

    var bufferSize = 4096;
    // record only 1 channel
    var recorder = audio_context.createScriptProcessor(bufferSize, 1, 1);
    // specify the processing function
    recorder.onaudioprocess = this.recorderProcess;
    // connect stream to our recorder
    audioInput.connect(recorder);
    // connect our recorder to the previous destination
    recorder.connect(audio_context.destination);
  };

  recorderProcess = e => {
    let sourceAudioBuffer = e.inputBuffer; // directly received by the audioprocess event from the microphone in the browser

    let TARGET_SAMPLE_RATE = 16000;
    let offlineCtx = new OfflineAudioContext(
      sourceAudioBuffer.numberOfChannels,
      sourceAudioBuffer.duration *
        sourceAudioBuffer.numberOfChannels *
        TARGET_SAMPLE_RATE,
      TARGET_SAMPLE_RATE
    );
    let buffer = offlineCtx.createBuffer(
      sourceAudioBuffer.numberOfChannels,
      sourceAudioBuffer.length,
      sourceAudioBuffer.sampleRate
    );
    // Copy the source data into the offline AudioBuffer
    for (
      let channel = 0;
      channel < sourceAudioBuffer.numberOfChannels;
      channel++
    ) {
      buffer.copyToChannel(sourceAudioBuffer.getChannelData(channel), channel);
    }
    // Play it from the beginning.
    let source = offlineCtx.createBufferSource();
    source.buffer = sourceAudioBuffer;
    source.connect(offlineCtx.destination);
    source.start(0);
    offlineCtx.oncomplete = e => {
      // `resampled` contains an AudioBuffer resampled at 16000Hz.
      // use resampled.getChannelData(x) to get an Float32Array for channel x.
      let resampled = e.renderedBuffer;
      let leftFloat32Array = resampled.getChannelData(0);
      //console.log(leftFloat32Array);
      socket.emit("audioData", this.convertFloat32ToInt16(leftFloat32Array));
      // use this float32array to send the samples to the server or whatever
    };
    offlineCtx.startRendering();
  };

  convertFloat32ToInt16 = buffer => {
    let l = buffer.length;
    let buf = new Int16Array(l);
    while (l--) {
      buf[l] = Math.min(1, buffer[l]) * 0x7fff;
    }
    // ////console.log(buf);
    return buf.buffer;
  };
  handleStartRecording = () => {
    socket.emit("startAudioStream");
    let soundCountPresent = 0;
    let soundCountPrevious = 0;
    var record = false;
    if (!record) {
      var l = setInterval(() => {
        record = true;
        if (soundCountPresent !== soundCountPrevious) {
          soundCountPresent = soundCountPrevious;
        } else {
          this.handleStopRecording();
          soundCountPresent = 0;
          soundCountPrevious = 0;
          record = false;
          this.setState({
            isMicOn: null
          });
          clearInterval(l);
        }
      }, 5000);
    }
    socket.on("result", msg => {
      console.log(msg);
      if (msg.length > 0) {
        this.setState({ text: msg });
      }
      soundCountPresent = soundCountPresent + 1;

      this.setState({
        saveStatus: "All changes saved"
      });
    });
    let constraints = {
      audio: true
    };
    navigator.getUserMedia(constraints, this.initializeRecorder, function(
      a,
      b,
      c
    ) {
      // console.log('abcd');
    });

    this.setState({ saveStatus: "Saving..." });
  };

  handleStopRecording = () => {
    socket.emit("stopAudioStream");
    mediaStream.getAudioTracks()[0].stop();
    //    audio_context.close();
  };

  render() {
    return (
      <div>
        <button
          onClick={this.handleToggle}
          style={{
            height: "2rem ",
            width: "5rem",
            margin: "5rem",
            fontSize: "1rem"
          }}
        >
          {this.state.isMicOn ? "Stop" : "Start"}
        </button>
        <br />
        {this.state.text}
      </div>
    );
  }
}
export default GnaniEditor;
