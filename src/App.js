import React from "react";

import "./App.css";
import GnaniEditor from "./editor";

function App() {
  return (
    <div className="App">
      <GnaniEditor />
    </div>
  );
}

export default App;
