'use strict'

//connection to socket
const socket = io.connect("https://demospeech.gnani.ai");
console.log("check");
let firstInitDone = false;
let sessionId = -1;
var botResponseCbk;
var speechTranscriptCbk;
//================= CONFIG =================
// Stream Audio
//let bufferSize = 2048,
let bufferSize = 2048,
	AudioContext,
	context,
	processor,
	input,
	globalStream;

//vars
let audioElement = document.querySelector('audio'),
	finalWord = false,
	resultText = document.getElementById('ResultText'),
	removeLastWord = true,
	streamStreaming = false,
	is_recording = false;


//audioStream constraints
const constraints = {
	audio: true,
	video: false
};


//================= RECORDING =================






function initRecording() {
        if (!firstInitDone)
        {
	socket.emit('startAudioStream', ''); //init socket Google Speech Connection
	streamStreaming = true;
	AudioContext = window.AudioContext || window.webkitAudioContext;
	context = new AudioContext();
	processor = context.createScriptProcessor(bufferSize, 1, 1);
	processor.connect(context.destination);
	context.resume();

	var handleSuccess = function (stream) {
		globalStream = stream;
		input = context.createMediaStreamSource(stream);
		input.connect(processor);

		processor.onaudioprocess = function (e) {
			microphoneProcess(e);
		};
	};

	navigator.mediaDevices.getUserMedia(constraints)
		.then(handleSuccess);
        firstInitDone = true;
    }
}

function microphoneProcess(e) {
	var left = e.inputBuffer.getChannelData(0);
	var left16 = convertFloat32ToInt16(left);
	socket.emit('audioData', left16);
}




//================= INTERFACE =================

// var endButton = document.getElementById("stopRecButton");
// endButton.addEventListener("click", stopRecording);
// endButton.disabled = true;
function toggleRecording(){
console.log("~toggleRecording");
	var text = startButton.value;
	if(text === "Speak"){
		startButton.value = "Stop";
		console.log("Invoking startRecording...")
		startRecording();
	}else{
		startButton.value = "Speak";
		stopRecording();
	}
}


function startRecording() {
	//startButton.value = "Stop";
	//startButton.disabled = true;
	//endButton.disabled = true;
	initRecording();
	socket.emit('startAudioStream', context.sampleRate);
	console.log("Recording Started");
}

function stopRecording() {
console.log("stopRecording~");
	streamStreaming = false;
	socket.emit('stopAudioStream', '');


}


function stopRecordingOnFormEnd() {
console.log("stopRecordingFormEnd~");
        $("img#start_button").prop("src", '/assets/img/mic.gif');
	streamStreaming = false;
	socket.emit('stopAudioStream', '');

        
	let track = globalStream.getTracks()[0];
	track.stop();

	input.disconnect(processor);
	processor.disconnect(context.destination);
	context.close().then(function () {
		input = null;
		processor = null;
		context = null;
		AudioContext = null;
		//startButton.disabled = false;
	});
    
}

//================= SOCKET IO =================
socket.on('connect', function (data) {
	socket.emit('join', 'Server Connected to Client');
});


socket.on('messages', function (data) {
	console.log(data);
	$("#my_data").append("<div class='chat self'><p class='chat-message'>" + data+ "</p></div>");

});

socket.on('response', function (data) {
	console.log(data);
	sessionId = data.botSessionId;
	console.log("Got Response from Server : " + data.responseText);

	$("#chat-parent").append("<div class='chat self'><div class='user-photo'><img src='/assets/img/you.png'></div><p class='chat-message'>" + data.responseText + "</p></div>");

//	botResponseCbk(data.responseText);
});

socket.on('speechData', function (data) {
	// console.log(data.results[0].alternatives[0].transcript);
	var dataFinal = undefined || data.results[0].isFinal;

	if (dataFinal === false) {

	} else if (dataFinal === true) {

		speechTranscriptCbk(data.results[0].alternatives[0].transcript);
	}
});








//================= SANTAS HELPERS =================

// sampleRateHertz 16000 //saved sound is awefull
function convertFloat32ToInt16(buffer) {
	let l = buffer.length;
	let buf = new Int16Array(l / 3);

	while (l--) {
		if (l % 3 == 0) {
			buf[l / 3] = buffer[l] * 0xFFFF;
		}
	}
	return buf.buffer
}

